const Footer = () => {
	const link = "https://www.augustinfotech.com";
	const target = "_blank";

	return (
		<div className="container">
			Copyright © <small>{new Date().getFullYear()}</small> August Infotech{" "}
			<a href={link} target={target}>
				www.augustinfotech.com
			</a>
		</div>
	);
};

export default Footer;
