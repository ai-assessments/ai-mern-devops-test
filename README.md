# 🤘 MERN CRUD app

MERN Stack CRUD built with React v17+, RRDv6+, Node.js, Express.js MongoDB and Mongoose ODM.

**You can import the JSON File named 'Cruds' to your MongoDB. You will find it in the Server Folder**

[Download](https://gitlab.com/ai-assessments/ai-mern-devops-test)**

## Installation

1. Clone repository

```shell
git clone https://gitlab.com/ai-assessments/ai-mern-devops-test
```

## Install Client (React Files)

2. Get in the client folder

```shell
cd client
```

3. Install dependencies via npm or yarn

```shell
npm i
```

4. Start Client

```shell
npm start
```

## Install Server (Node Files)

2. Get in the server folder

```shell
cd server
```

3. Install dependencies via npm or yarn

```shell
npm i
```

4. Start Server

```shell
nodemon server
```

## Install Database (JSON File)

2. Get in the server folder

```shell
cd server
```

3. Import the following file to your MongoDB

```shell
Cruds
```

**Thank you**
